<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model("options_model");
		$this->load->model("selections_model");

		$options 		= $this->options_model->get();

		foreach ($options as $key => $value) {
			$value->selections = $this->selections_model->get(array("option_index"=>$value->index));
		}

		$this->load->view('home_view',array("options"=>$options));
	}

	public function info($index,$title)
	{
		$this->load->model("info_model");
		$info_data = $this->info_model->get(array("index"=>$index));

		if(!empty($title))
		$info_data->title  = $title;

		$this->load->view("info_view", array('data'=>$info_data));
	}

	public function sendpdfemail($fname, $lname, $city, $state, $phone, $fromemail, $comments, $pdflink, $pdflink2){
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0'."\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
		$headers .= 'From: CJ Spray Rigs <info@cjsprayrigs.com>'."\r\n";

		$message = "PDF created by:<br /><br /><strong>First Name :</strong> $fname<br /><strong>Last Name :</strong> $lname<br /><strong>City :</strong> $city<br /><strong>State :</strong> $state<br /><strong>Phone :</strong> $phone<br /><strong>Email :</strong> $fromemail<br /><br /><strong>Comments :</strong><br />$comments<br /><br /><a href='$pdflink'>View PDF</a>";

		if($pdflink2){
			$message .= "<br /><a href='$pdflink2'>Dealer Copy</a>";
		}

		$email = "shenderson@cjspray.com";

		if(SITE == "idi"){
			$email = "afranzen@idimn.com, cwhitley@idimn.com";
		}

		// Mail it
		mail( $email, "Rig Builder PDF Created", $message, $headers );
	}

	public function createpdf(){
		$this->load->library("composite_image");
		$post = $this->input->post();
		$ts = time();

		$post["selections"] 	= json_decode($post["selections"], true);
		$post["price"] 			= json_decode($post["price"], true );
		$post["promo"] 			= json_decode($post["promo"], true );
		$post["dealer"]			= json_decode($post["dealer"], true );
		$post["comments"]		= $post["comments"];

		$dealer = empty( $post["dealer"] ) ? false : true;

		$image_url = base_url().$this->composite_image->generate( $post["selections"]["images"], $ts );

		$pdficon = base_url()."img/pdf-icon.jpg";

		$filename = 'myrig-page1-'. $ts;
		$filepath = FCPATH."/downloads/pdfs/$filename.pdf";

		$post['image_url'] = $image_url;

		if(file_exists($filepath) == FALSE){
			ini_set('memory_limit','32M');

			$this->load->library('pdf');
			$pdf = $this->pdf->load();

			//page 1: end user pdf has cjprice plus promo discounted price (you_price);
			$post["price_label"] = "Price"; 
			$post["price_value"] = "cj"; 
			$post["option_price_value"] = "list_price"; 
			$post["show_list_price"] = true;
			$post["show_footer"] = $dealer;


			if($dealer){
				//dealer type 2
				if( $post["dealer"]["type_id"] == 2 ){
					//dealer two page 1

					if( !empty( $post["profit_margin"] ) ) 
						$post["price_value"] = "graco"; 

					$dealertwo_pageone = $this->load->view('pdf_dealertwo_page_view', $post, true);
					$pdf->WriteHTML( $dealertwo_pageone );

					
				} else {
					//dealer one page 1

					if( !empty( $post["profit_margin"] ) ) 
						$post["price_value"] = "dealer"; 
					
					$dealerone_pageone = $this->load->view('pdf_dealerone_page_view', $post, true);
					$pdf->WriteHTML( $dealerone_pageone );
				}

				$pdf->Output( $filepath, 'F' );
				$url = base_url()."downloads/pdfs/$filename.pdf";
				echo "<a style='color:#999; text-align:center; text-decoration:none; display:inline-block;' href='$url' target='_blank'><img align='center' src='$pdficon' /><p>Customer Copy</p></a>";

				//dealer page 2

				if( $post["dealer"]["type_id"] == 2 ){
					$post["option_price_value"] = "graco_price"; 
					$post["price_value"] = "graco"; 
				} else {
					$post["price_value"] = "dealer"; 
					$post["option_price_value"] = "dealer_price"; 
				}

				$pdf2 = $this->pdf->load();
				$filename2 = 'myrig-page2-'. $ts;
				$filepath2 = FCPATH."/downloads/pdfs/$filename2.pdf";

				if(file_exists($filepath2) == FALSE){
					$post["price_label"] = SITE == "idi" ? "IDI Price" : "Graco Price"; 

					$graco_page = $this->load->view('pdf_dealer_pagetwo_view', $post, true);
					$pdf2->WriteHTML( $graco_page );

					$pdf2->Output( $filepath2, 'F' );

					$url2 = base_url()."downloads/pdfs/$filename2.pdf";

					echo "<a style='color:#999; text-align:center; text-decoration:none; display:inline-block;' href='$url2' target='_blank'><img align='center' src='$pdficon' /><p>Dealer Copy</p></a>";
				}

				//send email and add user to database
				$this->sendpdfemail( $post["fname"], $post["lname"], $post["city"], $post["state"], $post["phone"], $post["email"], $post["comments"], $url, $url2 );
			} else {
				$enduser_page = $this->load->view('pdf_nondealer_page_view', $post, true);
				$pdf->WriteHTML( $enduser_page );
				$pdf->Output( $filepath, 'F' );

				//send email and add user to database
				$this->sendpdfemail( $post["fname"], $post["lname"], $post["city"], $post["state"], $post["phone"], $post["email"], $post["comments"], base_url()."downloads/pdfs/$filename.pdf" );

				redirect( base_url()."downloads/pdfs/$filename.pdf" );
			}
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */