<? $this->load->view("pdf_css_view"); ?>

<?php 
	$cj_products = array();
	$graco_products = array();

	$cj_accessories = array();
	$graco_accessories = array();

	$cj_products_total = 0; 
	$graco_products_total = 0; 

	//options
	foreach ($selections["options"] as $key => $value) {
		if( intval( $value["value"]["product_type"]) == 2){
			array_push( $graco_products, $value );
			$graco_products_total += floatval( $value["value"][$option_price_value] );
		} else {
			array_push( $cj_products, $value );
			$cj_products_total += floatval( $value["value"][$option_price_value] );
		}
	}

	//accessories
	if( !empty( $selections["accessories"] ) ){
		foreach ($selections["accessories"] as $key => $value){
			if( intval( $value["product_type"]) == 2){
				array_push( $graco_accessories, $value );
				$graco_products_total += floatval( $value[$option_price_value] );
			} else {
				array_push( $cj_accessories, $value );
				$cj_products_total += floatval( $value[$option_price_value] );
			}
		}
	}

	//part number
	$ph = "";
	foreach ($selections["options"] as $key => $value) {
		$ph.= ($key.$value["value"]["id"]."-");
	}

	$customer_price = floatval(  $price[$price_value]  );
	$discounted_customer_price = floatval(  $price[$price_value]  );

	if(SITE == "idi"){
		$cj_products_total = $cj_products_total/.95;
	}

	if($profit_margin){
		$customer_price = $customer_price/( 1-(intval($profit_margin)/100) );
		$discounted_customer_price = $discounted_customer_price/( 1-(intval($profit_margin)/100) );
	}

	if( !empty($discount) ){
		$discounted_customer_price = $customer_price - floatval($discount);
	}
?>

<? if( !empty($dealer) && !empty($dealer['logo']) ): ?>
<div id="header">
	<img height="100" src="<?= base_url(); ?>img/dealer_logos/<?= $dealer['logo']; ?>">
	<img style="float:right" src="<?= base_url(); ?>img/logo_<?php echo SITE; ?>.png">
</div>
<? endif; ?>

<div id="main">
	<?php if($company_name) : ?>
		<h3><span class="field-label">Customer #:</span> <?php echo $company_name; ?></h3>
	<?php else : ?>
		<textarea width="800"></textarea>
	<?php endif; ?>
	<?php if($persons_name) : ?>
		<h3><span class="field-label">Ship To Address:</span> <?php echo $persons_name; ?></h3>
	<?php else : ?>
		<textarea width="400"></textarea>
	<?php endif; ?>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
		<tr>
			<td colspan="2">
				<h2><?= substr($ph,0,-1); ?></h2>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top:20px;">
				<?php if(SITE == "idi") : ?>
					<h3>Customer Total <span style="padding-left:40px;">$<?= number_format( $discounted_customer_price, 0); ?></span></h3>
				<?php else : ?>
					<h3>CJ Dealer Price <span style="padding-left:40px;">$<?= number_format( $customer_price, 0); ?></span></h3>
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-top:20px; vertical-align:top; padding-right:20px;">
				<table id="options" cellspacing="0" cellpadding="0" border="0" width="100%">
					<?php if( SITE !='idi' ): ?>
					<tr>
						<td class="label">
							<h3>CJ Spray Total</h3>
						</td>
						<td class="price" colspan="2">
							<h3>$<?= number_format( $cj_products_total, 0); ?></h3>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<th colspan="2" class="label" style="padding-top:40px;">
							<h3>Options</h3>
						</th>
					</tr>
					<?php foreach ($cj_products as $key => $value):  ?>
						<?php 
							$pn = $value["value"]["part_number"];
							if(empty($pn)) $pn = "N/A";
						?>
					<tr>
						<td><span class="label"><?= $pn; ?>:<?= $value["label"]; ?> :</span><?= $value["value"]["label"]; ?></td>
						<td class="price">$<?= number_format($value["value"][$option_price_value]/(SITE == "idi" ? .95 : 1), 0); ?></td>
					</tr>
					<?php endforeach ?>
					<tr class="header-row">
						<th colspan="2" class="label" style="padding-top:40px;">
							<h3>Accessories</h3>
						</th>
					</tr>
					<?php foreach ($cj_accessories as $key => $value): ?>
						<?php 
							$pn = $value["part_number"];
							if(empty($pn)) $pn = "N/A";
						?>
					<tr>
						<td><span class="label"><?= $pn; ?>:<?= $value["label"]; ?></span></td>
						<td class="price">$<?= number_format($value[$option_price_value]/(SITE == "idi" ? .95 : 1)	, 0); ?></td>
					</tr>
					<?php endforeach ?>
				</table>
			</td>
			<td style="padding-top:20px; padding-left:20px;  border-left:1px solid #666; vertical-align:top">
				<table id="options" cellspacing="0" cellpadding="0" border="0" width="100%">
					<?php if( SITE !='idi' ): ?>
					<tr>
						<td class="label">
							<h3>Graco Total</h3>
							<h4>(<?= $dealer["type_id"] == 2 ? "40%" : "30%"; ?> Graco Dealer Discount)</h4>
						</td>
						<td class="price" colspan="2">
							<h3>$<?= number_format( $graco_products_total , 0); ?></h3>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<th colspan="2" class="label" style="padding-top:40px;">
							<h3>Options</h3>
						</th>
					</tr>
					<?php foreach ($graco_products as $key => $value):  ?>
						<?php 
							$pn = $value["value"]["part_number"];
							if(empty($pn)) $pn = "N/A";
						?>
					<tr>
						<td><span class="label"><?= $pn; ?>:<?= $value["label"]; ?> :</span><?= $value["value"]["label"]; ?></td>
						<?php 
							$graco_item_price = floatval( $value["value"][$option_price_value] );
							$graco_item_price_ratio = $graco_item_price/$graco_products_total;

							if(SITE == "idi"){
								$graco_item_price = ($customer_price-$cj_products_total)*$graco_item_price_ratio;

								if( strpos( $value["label"], "Reactor" ) !== false && !empty($discount) ){
									$graco_item_price -= $discount;
								}
							}
						?>

						<td class="price">$<?= number_format( $graco_item_price, 0 ); ?></td>
					</tr>
					<?php endforeach ?>
					<?php if( count($graco_accessories) > 0 ): ?>
					<tr class="header-row">
						<th colspan="2" class="label" style="padding-top:40px;">
							<h3>Accessories</h3>
						</th>
					</tr>
					<?php endif; ?>
					<?php foreach ($graco_accessories as $key => $value): ?>
						<?php 
							$pn = $value["part_number"];
							if(empty($pn)) $pn = "N/A";
						?>
					<tr>
						<td><span class="label"><?= $pn; ?>:<?= $value["label"]; ?></span></td>
						<td class="price">$<?= number_format($value[$option_price_value], 0); ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top:40px; text-align:center;">
				<img style="vertical-align:top" src="<?= $image_url; ?>" />
			</td>
		</tr>
		<tr>
			<td style="padding-top:40px;">
				<?php if(!empty($comments)) : ?>
				<h3>Comments</h3>
				<p><?= $comments; ?></p>
				<?php endif; ?>
			</td>
		</tr>
	</table>
</div>


<?php if($show_footer) $this->load->view("pdf_footer_view"); ?>