<style type="text/css">
	@page {
		margin-top: 0cm;  
		margin-left: 0cm;  
		margin-right: 0cm;
		margin-bottom: 0cm;    
	}
	body{
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		border-bottom:<?php echo SITE == 'idi' ? '#DD0000' : '#004A80'; ?> 20px solid;
		font-size:12px;
	}
	span.field-label{
		color: #333;
	}
	#main{
		padding:30px;
	}
	div#header{
		width: 100%;
		padding:40px 20px 15px 30px;
		background: <?php echo SITE == 'idi' ? '#000000' : '#FFFFFF'; ?>
	}
	div#header img{
		height: 100px;
	}
	div#non-dealer-header{
		background: <?php echo SITE == 'idi' ? '#000000' : '#FFFFFF'; ?>
		border-top:<?php echo SITE == 'idi' ? '#DD0000' : '#004A80'; ?> 12px solid;
		width: 100%;
		padding:20px 20px 15px 30px;
	}
	div#subhead{
		background: <?php echo SITE == 'idi' ? '#DD0000' : '#004A80'; ?>
		padding: 4px 4px 4px 34px;
		width: 100%;
		color: #FFF;
		font-weight: 100;
		font-size: 14px;
	}
	.price{
		text-align: right;
		vertical-align: top;
	}
	.label{
		text-align: left;
	}
	#accessories{
		padding-left: 20px;
	}
	h4{
		font-weight: 100;
	}
	div#footer{
		width: 100%;
		position: absolute !important;
		bottom:0 !important;
		text-align: center;
		padding-bottom: 20px;
	}
	textarea{
		background:none;
	}
</style>