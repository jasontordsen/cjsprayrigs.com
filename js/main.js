var main = {};
var selections = {};

main.price = {};

main.configuration_complete = false;
main.dealer = null;
main.promo = {code:"", discount:0};
main.current_pn = "";

jQuery( function($){
	$.validator.methods.email = function( value, element ) {
	  return this.optional( element ) || /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]/.test( value );
	}

	$('a[data-toggle="tab"]').on('shown', function (e) {
		main.current_tab_id 	= String(e.target);
		main.previous_tab_id	= String(e.relatedTarget);

	  	main.current_tab_id 	= main.current_tab_id.substring(main.current_tab_id.search("#")+1); // activated tab
	  	main.previous_tab_id 	= main.previous_tab_id.substring(main.previous_tab_id.search("#")+1); // previous tab

	  	main.isFinishTabActive();

	  	$("body").scrollTop(0);
	  	$("html").scrollTop(0);
	});

	$('.fancybox').fancybox();

	//click events
	$("#options .accordion-body button").click( main.optionSelected );
	$("#trailers li img").click( main.optionSelected );
	$("#accessories button").click( main.accessorySelected );
	$("span.checkmark").click( main.removeOptionClicked );
	$("#dealer-logout-btn").click(main.dealerLogOut);

	$('#dealer-login-form').submit(main.submitDealerLoginForm);
	$('#promo-code-form').submit(main.submitPromoCodeForm);

	////console.log("init, base url : " + base_url + ", total options : " + total_options);
	//console.log("init", all_options );

	if( build != undefined && build != "" ){
		main.selectoptionsbybuildnumber( build );
		main.updateAll();
		main.getSession();
	} else {
		main.toInCompleteState();
		main.getSession();
	}

	window.onhashchange = function( e ){
		onhash( window.location.hash || "#all" );
	}

	$("input#profit-margin").blur(function(){
		var val = $(this).val();
		$(this).val( parseFloat(Math.round(val * 100) / 100).toFixed(2) );
	});

	$("input#discount").blur(function(){
		var val = $(this).val();
		$(this).val( parseFloat(Math.round(val * 100) / 100).toFixed(2) );
	});

	onhash( window.location.hash || "#all" );
	//?build=B-A:CC-4:D-1:EE-0:F-1:GG-0:H-0:II-1:J-2:KK-1:L-0:MM-0:N-0
});

/* =================================
   ==== WORKERS =================
   ================================= */

function onhash( _hash ){
	var hashval = _hash.substr(1);

	$("#trailers .thumbnails").isotope({ 
		itemSelector: '.trailer',
		masonry: {
			columnWidth: '.trailer'
		},
  		filter: hashval == "all" ? "" : '.' + hashval 
  	});

	$(".filter-navigation>li>a").each(function(){
		if( $(this).attr("href") == _hash && !$(this).parent().hasClass("active") ){
			$(this).parent().addClass("active");
		} else {
			$(this).parent().removeClass("active");
		}
	});
}

main.selectoptionsbybuildnumber = function( _build_number ){
	main.resetAll();

	$.each( build, function( _i, _v ){
		var option = getoptionbyid( _v.split("-")[0] );
		var selection = getselectionbyid( _v.split("-")[1] );

		function getoptionbyid( _op_id ){
			var op = null;

			$.each(all_options, function(_op_i, _op){
				if(_op.id == _op_id){
					op = _op;
				}
			});

			return op;
		}

		function getselectionbyid( _sel_id ){
			var sel = null;

			$.each(option.selections, function(_sel_i, _sel){
				if(_sel.id == _sel_id){
					sel = _sel;
				}
			});

			return sel;
		}

		selections.options[ option.id ] = { 
			index: option.index, 
			label: decodeURIComponent(option.label),
			value:
			{
				id: selection.id, 
				cj_price: Number( selection.cj_price ), 
				list_price: Number( selection.list_price ), 
				dealer_price: Number( selection.dealer_price ),
				graco_price: Number( selection.graco_price ),
				label: decodeURIComponent( selection.description ),
				dealer_only: selection.dealer_only,
				product_type: selection.product_type,
				part_number: selection.part_number
			}
		};
	} );

	console.log("selections: ", selections);

	main.activateTab("complete");
}

main.download = function(){
	var createpdfform = $("form#createpdf");

	createpdfform.children('input#pdf-selections').eq(0).attr( "value", JSON.stringify(selections) );
	createpdfform.children('input#pdf-price').eq(0).attr( "value", JSON.stringify(main.price) );
	createpdfform.children('input#pdf-promo').eq(0).attr( "value", JSON.stringify(main.promo) );
	createpdfform.children('input#pdf-dealer').eq(0).attr( "value", JSON.stringify(main.dealer) );

	//dealer fields
	createpdfform.children('input#pdf-company-name').eq(0).attr( "value", $("#company-name").val() );
	createpdfform.children('input#pdf-persons-name').eq(0).attr( "value", $("#persons-name").val() );
	createpdfform.children('input#pdf-profit-margin').eq(0).attr( "value", $("#profit-margin").val() );
	createpdfform.children('input#pdf-discount').eq(0).attr( "value", $("#discount").val() );

	//user fields
	createpdfform.children('input#pdf-fname').eq(0).attr( "value", $("input#fname").val() );
	createpdfform.children('input#pdf-lname').eq(0).attr( "value", $("input#lname").val() );
	createpdfform.children('input#pdf-city').eq(0).attr( "value", $("input#city").val() );
	createpdfform.children('input#pdf-state').eq(0).attr( "value", $("input#state").val() );
	createpdfform.children('input#pdf-phone').eq(0).attr( "value", $("input#phone").val() );
	createpdfform.children('input#pdf-email').eq(0).attr( "value", $("input#email").val() );		
	createpdfform.children('input#pdf-comments').eq(0).attr( "value", $("textarea#comments").val() );

	// $("form#userform").validate();

	if( $("form#userform").valid() ){
		// createpdfform.submit(function(e){
		// 	e.preventDefault();
		// 	console.log( createpdfform.serialize() );
		// });

		createpdfform.submit();
	}

    return false;
}

main.startOver = function(){
	main.resetAll();
	main.activateTab("trailers");

    return false;
}

main.resetAll = function(){
	$(".btn.active").removeClass("active");
	selections = {options:{}, accessories:[]};
	main.updateAll();
	main.updateAccessoryItems();
}

main.updateAll = function(){
	main.updateMenuSelections();
	main.updateImages();
	main.updatePartNumber();
	main.updateOptionItems();
	main.updatePrice();
	main.updateExcludes();
	main.checkComplete();
	main.updateEmptyOptions();

	//console.log("updateAll:",selections);
}

main.submitPromoCodeForm = function(){
	//console.log("submitPromoCodeForm");
	$.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: "json",
        success: function (response) {
        	//console.log(response);

            if(response.success){
            	main.promo.code = response.code;
            	main.promo.discount = response.discount;

            	if( !$("body").hasClass("promo") )
            		$("body").addClass("promo")

            	main.updatePrice();
            }else{
            	main.invalidPromoCode();
            }
        },
        error:function(error){
        	main.invalidPromoCode();
        }
    });

    return false;
}

main.invalidPromoCode = function(){
	alert("invalid promo code");
}

main.submitDealerLoginForm = function(){
	$.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: "json",
        success: function (response) {
            if(response.index){
            	main.dealer = response;
            	main.toLoggedInState();
            }else if(response == "failed"){
            	main.loginFailed();
            }
        },
        error:function(error){
        	main.loginFailed();
        }
    });

    return false;
}

main.loginFailed = function(){
	alert("login failed");
}

main.getSession = function(){
	$.ajax({
        type: 'POST',
        url: base_url + "auth/session",
        dataType: "json",
        success: function (response) {
            if(response.index && response.index != undefined){
            	main.dealer = response;
            	main.toLoggedInState();
            }else{
            	main.toLoggedOutState();
            	////console.log("no session");
            }
        }
    });
}

main.dealerLogOut = function(){
	$.ajax({
        type: 'POST',
        url: base_url + "auth/logout",
        success: function (response) {
            if(response == "success"){
            	main.dealer = null;
            	main.toLoggedOutState();
            }
        }
    });
}

main.removeOptionClicked = function(e){
	//console.log(e.target);

	var _this = $(this);
	var _option_id = _this.parent().parent().parent().attr("data-id");
	main.removeSelection(_option_id);
}

main.toLoggedInState = function(){
	$("#dealer-login-form").hide();
	$("#user-info").show();
	$("#user-info label").text( main.dealer.company_name );

	if( !$("body").hasClass("dealer") )
		$("body").addClass("dealer");

	main.updatePrice();
	main.updateOptionItems();

	//enable dealer only buttons
	main.showDealerOnly();

	$("input[dealer-not-required]").removeAttr("required");
}

main.toLoggedOutState = function(){
	//console.log("toLoggedOutState-");

	$("#dealer-login-form").show();
	$("#user-info").hide();
	$("#company-name").text("");

	if( $("body").hasClass("dealer") )
		$("body").removeClass("dealer");

	main.updatePrice();
	main.updateOptionItems();

	//disable dealer only buttons
	main.hideDealerOnly();

	//remove current dealer only selections
	main.removeDealerOnlySelections();

	$("input[dealer-not-required]").attr("required","required");
}

main.hideDealerOnly = function(){
	$(".btn-group .choice[data-dealer-only=1]").addClass("disabled");
	$(".dealer-only").css("display","none");
}

main.showDealerOnly = function(){
	$(".btn-group .choice[data-dealer-only=1].disabled").removeClass("disabled");
	$(".dealer-only").css("display","inline-block");
}

main.activateTab = function(_id){
	$('#pageTabs a[href="#' + _id + '"]').tab('show');
}

main.isFinishTabActive = function(){
	if( main.current_tab_id == "complete" ){
		if( !$("#main-container").hasClass("complete") )
			$("#main-container").addClass("complete");
	} else{
		if( $("#main-container").hasClass("complete") )
			$("#main-container").removeClass("complete");
	}
}

main.optionSelected = function(e){
	_this 	= $(this);

	if(_this.parent().hasClass("thumbnail"))
		_this 	= $(this).parent("div.thumbnail").parent("li.trailer");

	_option_index 	= decodeURIComponent( _this.attr("data-option-index") );
	_option_id 		= decodeURIComponent( _this.attr("data-option-id") );
	_option_label	= decodeURIComponent( _this.attr("data-option-label") );
	_id 			= decodeURIComponent( _this.attr("data-id") );
	_cj_price		= Number(_this.attr("data-cj-price"));
	_list_price		= Number(_this.attr("data-list-price"));
	_dealer_price	= Number(_this.attr("data-dealer-price"));
	_graco_price	= Number(_this.attr("data-graco-price"));
	_label 			= decodeURIComponent( _this.attr("data-label") );
	_dealer_only	= Number( _this.parent().attr("data-dealer-only") );
	_product_type	= _this.attr("data-product-type");
	_part_number	= _this.attr("data-part-number");

	if( !selections.options || selections.options == undefined )
	 	selections.options = {};

	selections.options[_option_id] = { 
		index: _option_index, 
		label: _option_label,
		value:
		{
			id: _id, 
			cj_price: _cj_price, 
			list_price: _list_price, 
			dealer_price: _dealer_price,
			graco_price: _graco_price,
			label: _label,
			dealer_only:_dealer_only,
			product_type:_product_type,
			part_number:_part_number
		}
	};

	main.updateAll();

	//if trailer
	if(_option_id == "B")
		main.activateTab("options");

	//close menus
	$(".accordion-body.collapse.in").collapse("hide");
}

main.updateImages = function(){
	selections.images = [];

	$(".image-container").children().remove();

	var trailer = "";
	$.each(selections.options, function( _i, _v ){
		var ext = "png";
		var src = "";

		if(_i == "B"){
			trailer = "B-" + _v.value.id;
			ext = "jpg";
			
			src = base_url + "img/" + trailer + "." + ext;
		} else {
			if(trailer != ""){
				src = base_url + "img/" + trailer + "_" + _i + "-" + _v.value.id + "." + ext;
			}
		}

		selections.images.push( {type: ext, filename:src} );

		var _image = new Image();
		_image.onerror = function(e){
			e.target.src = base_url + "img/blank.png";
		}
		_image.onload = function(e){
			$(".image-container").append(e.target);
		}

		_image.src = src;

		//console.log("image : " + _i + "-" + _v.value.id);
		////console.log(selections.images);
	});
}

main.updateMenuSelections = function(){
	//remove all first
	$("div.accordion-group").each(function(){
		var _this = $(this);
		var _a = _this.find(".accordion-heading a");

		if(_this.hasClass("selected"))
			_this.removeClass("selected");

		_a.find("span.selection").text("");
	});

	$.each(selections.options, function( _i, _v ){
		var acc_group = $("div.accordion-group[data-id="+_i+"]");
		var acc_a = acc_group.find(".accordion-heading a");

		if(!acc_group.hasClass("selected"))
			acc_group.addClass("selected");

		acc_a.find("span.selection").text( _v.value.label );
	});
}

main.removeSelection = function( _option_id ){
	//console.log("removing selection : " + _option_id);

	if( selections.options[_option_id] && 
		selections.options[_option_id].value )
	{
		delete selections.options[_option_id];

		main.updateAll();
	}
	
	$("div.accordion-group[data-id='" + _option_id + "'] button.active").removeClass("active");
}

main.removeDealerOnlySelections = function(){
	if(selections.options)
	$.each(selections.options, function( _i, _v ){
		if(_v.value.dealer_only == 1){

			main.removeSelection(_i);
		}
	});
}

main.accessorySelected = function(){
	selections.accessories = [];

	setTimeout(function(){
		$("#accessories button").each(function(){
			var _this = $(this);

			if(_this.hasClass("active")){
				selections.accessories.push(
				{
					id: decodeURIComponent( _this.attr("data-id") ),
					label: decodeURIComponent( _this.attr("data-label") ),
					list_price: Number(_this.attr("data-list-price")), 
					cj_price: Number(_this.attr("data-cj-price")), 
					dealer_price: Number(_this.attr("data-dealer-price")),
					graco_price: Number(_this.attr("data-graco-price")),
					product_type: Number(_this.attr("data-product-type")),
					part_number: _this.attr("data-part-number")
				});
			}
		});

		main.updateAccessoryItems();
		main.updatePrice();
	},100);
}

main.updateAccessoryItems = function(){
	$(".configuration-content .accessories li").remove();

	$.each(selections.accessories, function( _i, _v ){
		var ul 	= $(".configuration-content .accessories"),
		li 		= $("<li/>").attr("data-id", _v.id ),
		val 	= $("<span/>").text( _v.label ),
		price 	= $("<span/>").text("$"+ $.strToCommaDelimNumber(_v.list_price.toFixed(0)) ).addClass("price");

		val.appendTo(li);
		price.appendTo(li);
		li.appendTo(ul);
	});
}

main.updateOptionItems = function(){
	$(".configuration-content .items li").remove();

	if(selections.options){
		$.each(selections.options, function( _i, _v ){
			var ul 	= $(".configuration-content .items"),
			li 		= $("<li/>").attr("data-id", _i ).attr("data-index", _v.index ),
			label 	= $("<span/>").text( _v.label + ": ").addClass("lbl"),
			val 	= $("<span/>").text( _v.value.label ),
			price 	= $("<span/>").text("$"+ $.strToCommaDelimNumber( _v.value[main.dealer ? main.dealer.type_id == "2" ? "graco_price" : "dealer_price" : "list_price" ].toFixed(0) ) ).addClass("price");

			label.appendTo(li);
			val.appendTo(li);
			price.appendTo(li);
			li.appendTo(ul);
		});
		main.checkConfigurationSelections();
	}

	

	$.sortItemsByAttribute("#mini-configuration .items li", "data-index", true);
	$.sortItemsByAttribute("#mini-configuration .items li", "data-index", true);
}

main.updatePartNumber = function(){
	//remove all first
	$(".configuration-content .part-number li").each(function(){
		$(this).children("span").remove();

		if(!$(this).hasClass("inactive")){
			$(this).addClass("inactive");
		}
	});

	main.current_pn = "";

	$.each(selections.options, function( _i, _v ){
		li 		= $(".configuration-content .part-number li[data-id=" + _i + "]"),
		label 	= $("<span/>").text( _i ).addClass("lbl"),
		val 	= $("<span/>").text( _v.value.id );

		if( li.hasClass("inactive") ){
			li.removeClass("inactive");
		}

		label.appendTo(li);
		val.appendTo(li);

		main.current_pn += (_i + "-" + _v.value.id + ":");
		
	});

	//console.log(main.current_pn);

	$.sortItemsByAttribute("#mini-configuration .part-number li", "data-index", true);
	$.sortItemsByAttribute("#full-configuration .part-number li", "data-index", true);

	////console.log("total part numbers : " + $("#mini-configuration .part-number li").length );
}

main.updatePrice = function(){
	main.price.list = 0;
	main.price.dealer = 0;
	main.price.cj = 0;
	main.price.graco = 0;


	if(selections.options){
		$.each(selections.options, function( _i, _v ){
			main.price.list += Number(_v.value.list_price);
			main.price.dealer += Number(_v.value.dealer_price);
			main.price.cj += Number(_v.value.cj_price);
			main.price.graco += Number(_v.value.graco_price);
		});
	}
	
	if(selections.accessories){
		$.each(selections.accessories, function( _i, _v ){
			main.price.list += Number(_v.list_price);
			main.price.dealer += Number(_v.dealer_price);
			main.price.cj += Number(_v.cj_price);
			main.price.graco += Number(_v.graco_price);
		});
	}

	main.price.list 	= main.price.list.toFixed(0);
	main.price.dealer 	= main.price.dealer.toFixed(0);
	main.price.cj 		= main.price.cj.toFixed(0);
	main.price.graco	= main.price.graco.toFixed(0);

	$(".configuration-content .list-price span").text( "$" + $.strToCommaDelimNumber(main.price.list) );
	
	var label = SITE == "idi" ? "IDI Price" : main.dealer ? main.dealer.type_id == "2" ? "Graco Dealer Price" : "Dealer Price" : "CJ Price";
	var price = main.dealer ? main.dealer.type_id == "2" ? main.price.graco : main.price.dealer : main.price.cj;

	main.price.you =  main.price.cj - Math.round(main.price.cj*main.promo.discount);

	$(".configuration-content .cj-price h3").html( label + "<span></span>");
	$(".configuration-content .cj-price span").text( "$" + $.strToCommaDelimNumber(price) );
	$(".configuration-content .promo-code h4").html("Promo Discount : " + String(main.promo.code) + "<span></span>");
	$(".configuration-content .promo-code h4 span").text( main.promo.discount * 100 + "%");
	$(".configuration-content .your-price span").text( "$" + $.strToCommaDelimNumber(main.price.you.toFixed(0)) );
}

main.updateExcludes = function(){
	$("div.choice").css("display","block");
	$("div.choice").attr( "disabled", false );

	$("div.accessory").css("display","block");
	$("div.accessory").attr( "disabled", false );

	$.each(selections.options, function( _i, _v ){
		var key = (_i + "-" + _v.value.id).toUpperCase();

		$("div.choice." + key ).each(function(){
			var choice_btn = $(this).children("button").eq(0);
			var choice_id = choice_btn.attr("data-id");
			var choice_option_id = choice_btn.attr("data-option-id");

			//hide it
			$(this).css( "display", "none" );
			$(this).attr( "disabled", true );

			//remove it from selections if it's active.
			if( choice_btn.hasClass("active") )
				main.removeSelection(choice_option_id);

			////console.log( choice_option_id + ":" + choice_id + ":" + choice_btn.hasClass("active") );
		});

		$("div.accessory." + key ).each(function(){
			var accessory_btn = $(this).children("button").eq(0);
			var accessory_id = accessory_btn.attr("data-id");
			var accessory_option_id = accessory_btn.attr("data-option-id");

			//hide it
			$(this).css( "display", "none" );
			$(this).attr( "disabled", true );

			//remove it from selections if it's active.
			if( accessory_btn.hasClass("active") )
				main.removeSelection(accessory_option_id);

			////console.log( choice_option_id + ":" + choice_id + ":" + choice_btn.hasClass("active") );
		});
	});
}

main.updateEmptyOptions = function(){
	console.log(selections);

	$("#options .accordion-group").each(function(){
		var choices = $(this).find(".choice"), empty = true;

		choices.each(function(){
			// console.log($(this).attr("disabled"));

			if( !$(this).attr("disabled") ){
				empty = false;
			}
		});

		$(this).css("display", empty ? "none" : "block");

		console.log(empty);
	});
}

main.checkConfigurationSelections = function(){
	if( Object.keys( selections.options ).length > 0 ){
		if( $("#main-container").hasClass("no-selections") )
			$("#main-container").removeClass("no-selections");
	} else{
		if( !$("#main-container").hasClass("no-selections") )
			$("#main-container").addClass("no-selections");
	}
}

main.toCompleteState = function(){
	if( !$("body").hasClass("complete") ){
		$("body").addClass("complete");
	}

	$(".require-complete").attr("disabled",false);
}

main.toInCompleteState = function(){
	if( $("body").hasClass("complete") ){
		$("body").removeClass("complete");
	}

	$(".require-complete").attr("disabled",true);
}

main.checkComplete = function(){
	if( selections.options ){
		main.configuration_complete = true; //Object.keys(selections.options).length == total_options;

		if( main.configuration_complete ){
			main.toCompleteState();
		} else {
			main.toInCompleteState();
		}
	}
}